# P0 (Guitar Shop) by Kota Nishigaki

The guitar shop API that allows users to sign up, login, view products (guitars), manage cart and purchase items.

- Users' passwords are hashed (PBKDF2) before storing.

- Logged in users and admin routes are protected through the use of JWT.

- A lineup of products can be managed only by an admin user through the protected admin route.


## - Basic Routes
/guitars  
/users  
/admin  

## - Examples (/guitars)

### GET /guitars  
Get all items from the shop:
```javascript
[
     {
        "id": 1,
        "maker": "Fender",
        "name": "PM-1 Standard Dreadnought, Natural",
        "price": 829.99,
        "condition": "new"
    },
    {
        "id": 2,
        "maker": "Martin",
        "name": "D-28",
        "price": 2899.0,
        "condition": "new"
    },

            .
            .
            .
]
```

### GET /guitars/{guitar_id}   
Get one item from the shop:
```javascript
[
     {
        "id": 1,
        "maker": "Fender",
        "name": "PM-1 Standard Dreadnought, Natural",
        "price": 829.99,
        "condition": "new"
    }
]
```
### GET /guitars{query strings}  
Available queries:
  - max-price  
  - min-price  
  - maker  
  - condition

Example: /guitars?maker=gibson&max-price=1500&condition=used
```javascript
[
    {
        "id": 28,
        "maker": "Gibson",
        "name": "L-00 Standard",
        "price": 1399.0,
        "condition": "used"
    },
    {
        "id": 30,
        "maker": "Gibson",
        "name": "J-45 Studio Rosewood",
        "price": 949.0,
        "condition": "used"
    }
]
```

## - Examples (/users)

### POST /users/signup  
Sign up user with email and password.
- Provide email and password in the body of your POST request as a form

### POST /users/login
Login user with email and password.
1. Provide email and password in the body of your POST request as a form
2. Copy the JWT returned
3. Set a header with the key as "Authorization" and value as the copied JWT in order to access protected user routes

### GET /users/{user_id}/cart
Get all items in the user's cart (JWT authorization required):
```javascript
[
    {
        "id": 11,
        "name": "PM-1 Standard Dreadnought, Natural",
        "price": 829.99,
        "quantity": 1
    },
    {
        "id": 20,
        "name": "ARTIST MOSAIC ANTHEM EQ",
        "price": 850.0,
        "quantity": 1
    },
    {
        "id": 0,
        "name": "Total Price",
        "price": 1679.99,
        "quantity": 1
    }
]
```

### POST /users/{user_id}/cart/{guitar_id}
Add item to the user's cart (JWT authorization required).  
- If the item already exists in the cart, it adds 1 to the quantity
- Quantity can be specified through a query string (i.e. "/...?quantity=10") otherwise defaults to 1

### DELETE /users/{user_id}/cart/{guitar_id}
Delete item from the user's cart (JWT authorization required).

### POST /users/{user_id}/purchase
Purchase items in the user's cart (JWT authorization required).
- The cart will be emptied
- The purchase invoice will be stored in the database


## - Examples (/admin)

### POST /admin/guitars
Add a new item to the shop (JWT authorization as an admin required).
- Send a new item in the request body in a json format:
```javascript
{
    "maker": "Martin",
    "name": "D-28",
    "price": 2899.00,
    "condition": "new"
}
```

### PUT /admin/guitars/{guitar_id}
Update item in the shop.
- Send a updated item in the request body in a json format:
```javascript
{
    "maker": "Martin",
    "name": "D-28",
    "price": 199.00,
    "condition": "used"
}
```
### DELETE /admin/guitars/{guitar_id}
Delete item from the shop.