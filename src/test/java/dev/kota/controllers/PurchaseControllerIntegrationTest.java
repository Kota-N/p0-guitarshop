package dev.kota.controllers;

import dev.kota.App;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.*;

public class PurchaseControllerIntegrationTest {

    private static final App app = new App();

    @BeforeAll
    public static void startService() {
        app.start(7000);
    }

    @AfterAll
    public static void stopService() {
        app.stop();
    }

    @Test
    void testGetCartAuthorized() {
        HttpResponse<String> response = Unirest.get("http://localhost:7000/users/1/cart").asString();

        assertAll(() -> assertEquals(401, response.getStatus()),
                () -> assertEquals("Unauthorized", response.getBody()));
    }

    @Test
    void testAddToCartAuthorized() {
        HttpResponse<String> response = Unirest.post("http://localhost:7000/users/1/cart/1").asString();

        assertAll(() -> assertEquals(401, response.getStatus()),
                () -> assertEquals("Unauthorized", response.getBody()));
    }

    @Test
    void testDeleteFromCartAuthorized() {
        HttpResponse<String> response = Unirest.delete("http://localhost:7000/users/1/cart/1").asString();

        assertAll(() -> assertEquals(401, response.getStatus()),
                () -> assertEquals("Unauthorized", response.getBody()));
    }

    @Test
    void testPurchaseAuthorized() {
        HttpResponse<String> response = Unirest.post("http://localhost:7000/users/1/purchase").asString();

        assertAll(() -> assertEquals(401, response.getStatus()),
                () -> assertEquals("Unauthorized", response.getBody()));
    }
}
