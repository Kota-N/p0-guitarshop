package dev.kota.controllers;

import dev.kota.App;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.*;


public class GuitarControllerIntegrationTest {

    private static final App app = new App();

    @BeforeAll
    public static void startService() {
        app.start(7000);
    }

    @AfterAll
    public static void stopService() {
        app.stop();
    }


    @Test
    void testPostNewGuitarAuthorized() {
        HttpResponse<String> response = Unirest.post("http://localhost:7000/admin/guitars").asString();

        assertAll(() -> assertEquals(401, response.getStatus()),
                () -> assertEquals("Unauthorized", response.getBody()));
    }

    @Test
    void testUpdateGuitarAuthorized() {
        HttpResponse<String> response = Unirest.put("http://localhost:7000/admin/guitars").asString();

        assertAll(() -> assertEquals(401, response.getStatus()),
                () -> assertEquals("Unauthorized", response.getBody()));
    }

    @Test
    void testDeleteGuitarAuthorized() {
        HttpResponse<String> response = Unirest.delete("http://localhost:7000/admin/guitars").asString();

        assertAll(() -> assertEquals(401, response.getStatus()),
                () -> assertEquals("Unauthorized", response.getBody()));
    }


}
