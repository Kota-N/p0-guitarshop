package dev.kota.controllers;

import dev.kota.models.Guitar;
import dev.kota.services.GuitarService;
import io.javalin.http.Context;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

public class GuitarControllerTest {

    private List<Guitar> getGuitars() {
        List<Guitar> guitars = new ArrayList<>();
        guitars.add(new Guitar(1, "Martin", "Test1", 1000.0, "new"));
        guitars.add(new Guitar(2, "Squire", "test2", 100.0, "new"));
        guitars.add(new Guitar(3, "Taylor", "Test3", 500.0, "used"));

        return guitars;
    }

    @InjectMocks
    private GuitarController guitarController;

    @Mock
    private GuitarService guitarService;

    @BeforeEach
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testGetAllGuitars() {
        Context ctx = mock(Context.class);

        List<Guitar> guitars = getGuitars();

        when(guitarService.getAll()).thenReturn(guitars);

        guitarController.handleGetGuitars(ctx);
        verify(ctx).json(guitars);
    }

    @Test
    void testGetFilteredItemsMaxPrice() {
        Context ctx = mock(Context.class);

        List<Guitar> guitars = getGuitars();

        when(ctx.queryParam("max-price")).thenReturn("800");
        when(guitarService.getFilteredItems("800", null, null, null)).thenReturn(guitars);

        guitarController.handleGetGuitars(ctx);
        verify(ctx).json(guitars);
    }

    @Test
    void testGetFilteredItemsMinPrice() {
        Context ctx = mock(Context.class);

        List<Guitar> guitars = getGuitars();

        when(ctx.queryParam("min-price")).thenReturn("500");
        when(guitarService.getFilteredItems(null, "500", null, null)).thenReturn(guitars);

        guitarController.handleGetGuitars(ctx);
        verify(ctx).json(guitars);
    }

    @Test
    void testGetFilteredItemsMaker() {
        Context ctx = mock(Context.class);

        List<Guitar> guitars = getGuitars();

        when(ctx.queryParam("maker")).thenReturn("Test");
        when(guitarService.getFilteredItems(null, null, "Test", null)).thenReturn(guitars);

        guitarController.handleGetGuitars(ctx);
        verify(ctx).json(guitars);
    }

    @Test
    public void testGetFilteredItemsCondition() {
        Context ctx = mock(Context.class);

        List<Guitar> guitars = getGuitars();

        when(ctx.queryParam("condition")).thenReturn("new");
        when(guitarService.getFilteredItems(null, null, null, "new")).thenReturn(guitars);

        guitarController.handleGetGuitars(ctx);
        verify(ctx).json(guitars);
    }

//
//      Getting NullPointerException
//    @Test
//    public void testPostNew() {
//        Context ctx = mock(Context.class);
//
//        Guitar newGuitar = new Guitar(1, "Yamaha", "DX-100", 500.0, "new");
//
//        when(ctx.bodyAsClass(Guitar.class)).thenReturn(newGuitar);
//        when(guitarService.addNew(newGuitar)).thenReturn(1);
//
//        guitarController.handlePostNewGuitar(ctx);
//        verify(ctx).result("Added new item: " + newGuitar.getName()).status(201);
//    }

}
