package dev.kota;

import dev.kota.controllers.GuitarController;
import dev.kota.controllers.PurchaseController;
import dev.kota.controllers.UserController;
import io.javalin.Javalin;

import static io.javalin.apibuilder.ApiBuilder.*;

public class App {

    GuitarController guitarController = new GuitarController();
    UserController userController = new UserController();
    PurchaseController purchaseController = new PurchaseController();

    Javalin app = Javalin.create().routes(() -> {
        path("/", () -> {
            get(ctx -> ctx.result("working!"));
            post(ctx -> ctx.result("I love tea.").status(418));
        });

        path("/guitars", () -> {
            get(guitarController::handleGetGuitars);

            path(":id", () -> {
                get(guitarController::handleGetGuitarById);
            });

        });

        path("/users", () -> {
            path("/signup", () -> {
                post(userController::handleSignUp);
            });

            path("/login", () -> {
                post(userController::handleLogin);
            });

            path(":userId", () -> {

                before(userController::authorizeUser);

                path("/cart", () -> {
                    get(purchaseController::handleGetCart);

                    path(":itemId", () -> {
                        post(purchaseController::handleAddToCart);
                        delete(purchaseController::handleDeleteFromCart);
                    });
                });

                path("/purchase", () -> {
                    post(purchaseController::handlePurchase);
                });

            });

        });


        path("/admin", () -> {

            before(userController::authorizeAdmin);

            path("/guitars", () -> {
                post(guitarController::handlePostNewGuitar);
                path(":id", () -> {
                    put(guitarController::handlePutGuitarById);
                    delete(guitarController::handleDeleteGuitarById);
                });
            });
        });
    });

    public void start(int port) {
        this.app.start(port);
    }

    public void stop() {
        this.app.stop();
    }

}
