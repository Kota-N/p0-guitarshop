package dev.kota.services;

import dev.kota.data.GuitarDao;
import dev.kota.data.GuitarDaoImpl;
import dev.kota.models.Guitar;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class GuitarService {
    private GuitarDao guitarDao = new GuitarDaoImpl();

    public List<Guitar> getAll() {
        return guitarDao.getAllGuitars();
    }

    public Guitar getById(int id) {
        return guitarDao.getGuitarById(id);
    }

    public int addNew(Guitar newGuitar) {
        return guitarDao.addNewGuitar(newGuitar);
    }

    public int putById(int id, Guitar updateGuitar) {
        return guitarDao.updateGuitarById(id, updateGuitar);
    }

    public int deleteById(int id) {
        return guitarDao.deleteGuitarById(id);
    }

    public List<Guitar> getFilteredItems(String maxPrice, String minPrice, String maker, String condition) {
        List<Guitar> guitars = guitarDao.getAllGuitars();

        if (maxPrice != null && maxPrice.matches("\\d+")) guitars = filterPrice(guitars, maxPrice, true);
        if (minPrice != null && minPrice.matches("\\d+")) guitars = filterPrice(guitars, minPrice, false);
        if (maker != null) guitars = filterMaker(guitars, maker);
        if (condition != null) guitars = filterCondition(guitars, condition);

        return guitars;
    }

    private List<Guitar> filterPrice(List<Guitar> guitars, String price, boolean isMax) {
        List<Guitar> filteredGuitars = new ArrayList<>();
        double priceFilter = Double.parseDouble(price);

        if (isMax) {
            for (Guitar guitar : guitars) {
                if (guitar.getPrice() <= priceFilter) filteredGuitars.add(guitar);
            }
        } else {
            for (Guitar guitar : guitars) {
                if (guitar.getPrice() >= priceFilter) filteredGuitars.add(guitar);
            }
        }

        return filteredGuitars;
    }

    private List<Guitar> filterMaker(List<Guitar> guitars, String maker) {
        List<Guitar> filteredGuitars = new ArrayList<>();

        for (Guitar guitar : guitars) {
            if (guitar.getMaker().equalsIgnoreCase(maker)) filteredGuitars.add(guitar);
        }

        return filteredGuitars;
    }

    private List<Guitar> filterCondition(List<Guitar> guitars, String condition) {
        List<Guitar> filteredGuitars = new ArrayList<>();

        if (condition.equalsIgnoreCase("new") || condition.equalsIgnoreCase("used")) {

            for (Guitar guitar : guitars) {
                if (guitar.getCondition().equalsIgnoreCase(condition)) filteredGuitars.add(guitar);
            }

            return filteredGuitars;
        }

        return guitars;
    }

}
