package dev.kota.services;

import dev.kota.data.PurchaseDao;
import dev.kota.data.PurchaseDaoImpl;
import dev.kota.models.CartItem;

import java.util.List;

public class PurchaseService {
    PurchaseDao purchaseDao = new PurchaseDaoImpl();

    public List<CartItem> getCart(int userId) {
        return purchaseDao.getCart(userId);
    }

    public int addToCart(int userId, int itemId, int quantity) {
        return purchaseDao.addToCart(userId, itemId, quantity);
    }

    public int addToCart(int userId, int itemId) {
        return purchaseDao.addToCart(userId, itemId);
    }

    public int deleteFromCart(int userId, int itemId) {
        return purchaseDao.deleteFromCart(userId, itemId);
    }

    public int purchase(int userId) {
        return purchaseDao.purchase(userId);
    }
}
