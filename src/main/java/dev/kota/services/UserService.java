package dev.kota.services;

import dev.kota.data.UserDao;
import dev.kota.data.UserDaoImpl;
import dev.kota.models.User;

public class UserService {
    private UserDao userDao = new UserDaoImpl();

    public User loginUser(String email, String password) {
        return userDao.loginUser(email, password);
    }

    public int signUp(String email, String password) {return userDao.signUp(email, password); }
}
