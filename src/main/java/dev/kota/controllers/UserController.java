package dev.kota.controllers;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import dev.kota.models.User;
import dev.kota.services.UserService;
import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import io.javalin.http.UnauthorizedResponse;
import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;


public class UserController {
    private final Logger logger = LoggerFactory.getLogger(UserController.class);
    UserService userService = new UserService();

    private byte[] hashPassword(final char[] password, final byte[] salt, final int iterations, final int keyLength) {

        try {
            SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512");
            PBEKeySpec spec = new PBEKeySpec(password, salt, iterations, keyLength);
            SecretKey key = skf.generateSecret(spec);
            return key.getEncoded();

        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            logger.error("Password hashing failed: {}", e);
            throw new RuntimeException(e);
        }
    }

    private String generateHashedString(String password) {

        String salt = "1234";
        int iterations = 10000;
        int keyLength = 120;
        char[] passwordChars = password.toCharArray();
        byte[] saltBytes = salt.getBytes();

        byte[] hashedBytes = hashPassword(passwordChars, saltBytes, iterations, keyLength);
        return Hex.encodeHexString(hashedBytes);
    }

    public void handleSignUp(Context ctx) {
        String email = ctx.formParam("email");
        String password = ctx.formParam("password");

        if (email == null || password == null) {
            logger.warn("Sign up failed: need both email and password");
            throw new BadRequestResponse("Please provide both email and password");
        }

        if (userService.signUp(email, generateHashedString(password)) > 0) {
            logger.info("Successfully signed up");
            ctx.result("Success").status(201);
        }

        else {
            logger.warn("Sign up failed");
            throw new BadRequestResponse("User already exists");
        }

    }

    public void handleLogin(Context ctx) {
        String email = ctx.formParam("email");
        String password = ctx.formParam("password");

        if (email == null || password == null) {
            logger.warn("Login failed: need both email and password");
            throw new BadRequestResponse("Please provide both email and password");
        }

        User user = userService.loginUser(email, generateHashedString(password));

        if (user != null) {

            try {
                Algorithm algorithm = Algorithm.HMAC256(System.getenv("jwtSecret"));
                String token;
                if (user.isAdmin()) {
                    token = JWT.create()
                            .withIssuer("guitar shop")
                            .withClaim("id", user.getId())
                            .withClaim("isAdmin", true)
                            .sign(algorithm);
                } else {
                    token = JWT.create()
                            .withIssuer("guitar shop")
                            .withClaim("id", user.getId())
                            .sign(algorithm);
                }

                logger.info("Successfully logged in");
                ctx.json(token);

            } catch (JWTCreationException exception) {
                //Invalid Signing configuration / Couldn't convert Claims.
                logger.warn("Failed to sign JWT");
                throw new BadRequestResponse("Something went wrong while signing JWT");
            }
        } else {
            logger.warn("Failed to login: wrong credentials");
            throw new UnauthorizedResponse("Failed to login");
        }
    }


    public void authorizeUser(Context ctx) {

        String token = ctx.header("Authorization");
        int userId = Integer.parseInt(ctx.pathParam("userId"));

        if (token != null) {

            try {
                Algorithm algorithm = Algorithm.HMAC256(System.getenv("jwtSecret"));
                JWTVerifier verifier = JWT.require(algorithm)
                        .withIssuer("guitar shop")
                        .build(); //Reusable verifier instance

                DecodedJWT jwt = verifier.verify(token);

                if (jwt.getClaim("id").asInt() != userId) {
                    logger.warn("User id in the path (user/:id) and user id in verified JWT do not match");
                    throw new UnauthorizedResponse("Unauthorized");
                }


            } catch (JWTVerificationException exception) {
                //Invalid signature/claims
                logger.warn("Failed to verify JWT");
                throw new UnauthorizedResponse("Unauthorized");
            }

        } else {
            logger.warn("No token provided");
            throw new UnauthorizedResponse("Unauthorized");
        }

    }

    public void authorizeAdmin(Context ctx) {

        String token = ctx.header("Authorization");

        if (token != null) {

            try {
                Algorithm algorithm = Algorithm.HMAC256(System.getenv("jwtSecret"));
                JWTVerifier verifier = JWT.require(algorithm)
                        .withIssuer("guitar shop")
                        .withClaim("isAdmin", true)
                        .build(); //Reusable verifier instance

                verifier.verify(token);

            } catch (JWTVerificationException exception) {
                //Invalid signature/claims
                logger.warn("Failed to verify JWT");
                throw new UnauthorizedResponse("Unauthorized");
            }

        } else {
            logger.warn("No token provided");
            throw new UnauthorizedResponse("Unauthorized");
        }

    }
}
