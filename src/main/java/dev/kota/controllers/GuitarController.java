package dev.kota.controllers;

import dev.kota.models.Guitar;
import dev.kota.services.GuitarService;
import io.javalin.http.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class GuitarController {
    private final Logger logger = LoggerFactory.getLogger(GuitarController.class);
    private GuitarService guitarService = new GuitarService();

    public void handleGetGuitars(Context ctx) {
        String maxPrice = ctx.queryParam("max-price");
        String minPrice = ctx.queryParam("min-price");
        String maker = ctx.queryParam("maker");
        String condition = ctx.queryParam("condition");

        if (maxPrice != null || minPrice != null || maker != null || condition != null) {
            logger.info("Getting filtered items");
            ctx.json(guitarService.getFilteredItems(maxPrice, minPrice, maker, condition));
        } else {
            logger.info("Getting all guitars");
            ctx.json(guitarService.getAll());
        }

    }

    public void handleGetGuitarById(Context ctx) {
        int id = Integer.parseInt(ctx.pathParam("id"));

        logger.info("Getting a guitar with id {}", id);
        ctx.json(guitarService.getById(id));
    }

    public void handlePostNewGuitar(Context ctx) {
        Guitar newGuitar = ctx.bodyAsClass(Guitar.class);

        if (guitarService.addNew(newGuitar) > 0) {
            logger.info("Adding new item: {}", newGuitar.getName());
            ctx.result("Added new item: " + newGuitar.getName()).status(201);
        }
        else {
            logger.warn("Failed to add an item");
            ctx.result("Failed to add an item").status(400);
        }
    }

    public void handlePutGuitarById(Context ctx) {
        int id = Integer.parseInt(ctx.pathParam("id"));
        Guitar updateGuitar = ctx.bodyAsClass(Guitar.class);
        if (guitarService.putById(id, updateGuitar) > 0) {
            logger.info("Item Updated: {}", updateGuitar.getName());
            ctx.result("Updated: " + updateGuitar.getName());
        }
        else {
            logger.warn("Failed to update an item");
            ctx.result("Failed to update an item").status(400);
        }
    }

    public void handleDeleteGuitarById(Context ctx) {
        int id = Integer.parseInt(ctx.pathParam("id"));
        if (guitarService.deleteById(id) > 0) {
            logger.info("Item Deleted: {}", id);
            ctx.result("Deleted");
        }
        else {
            logger.warn("Failed to delete an item");
            ctx.result("Failed to delete an item").status(400);
        }

    }
}
