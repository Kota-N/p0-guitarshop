package dev.kota.controllers;

import dev.kota.services.PurchaseService;
import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PurchaseController {
    private final Logger logger = LoggerFactory.getLogger(PurchaseController.class);
    private PurchaseService purchaseService = new PurchaseService();

    public void handleGetCart(Context ctx) {
        int userId = Integer.parseInt(ctx.pathParam("userId"));
        logger.info("Getting cart for user (id: {})", userId);
        ctx.json(purchaseService.getCart(userId));
    }

    public void handleAddToCart(Context ctx) {
        int userId = Integer.parseInt(ctx.pathParam("userId"));
        int itemId = Integer.parseInt(ctx.pathParam("itemId"));
        String quantity = ctx.queryParam("quantity");

        if (quantity != null && quantity.matches("\\d+")) {
            if (purchaseService.addToCart(userId, itemId, Integer.parseInt(quantity)) > 0) {
                logger.info("Item (id: {}) added to cart (user_id: {})", itemId, userId);
                ctx.result("Successfully added");
            }
            else {
                logger.warn("Failed to add item (id: {}) to cart for user (id: {})", itemId, userId);
                throw new BadRequestResponse("Couldn't add item to cart");
            }
        }
        else {
            if (purchaseService.addToCart(userId, itemId) > 0) {
                logger.info("Item (id: {}) added to cart (user_id: {})", itemId, userId);
                ctx.result("Successfully added");
            }
            else {
                logger.warn("Failed to add item (id: {}) to cart (user_id: {})", itemId, userId);
                throw new BadRequestResponse("Couldn't add item to cart");
            }
        }
    }

    public void handleDeleteFromCart(Context ctx) {
        int userId = Integer.parseInt(ctx.pathParam("userId"));
        int itemId = Integer.parseInt(ctx.pathParam("itemId"));

        if (purchaseService.deleteFromCart(userId, itemId) > 0) {
            logger.info("Item (id: {}) deleted from cart (user_id: {})", itemId, userId);
            ctx.result("Successfully deleted");
        }
        else {
            logger.warn("Failed to delete item (id: {}) from cart (user_id: {})", itemId, userId);
            throw new BadRequestResponse("Couldn't delete item from cart");
        }
    }

    public void handlePurchase(Context ctx) {
        int userId = Integer.parseInt(ctx.pathParam("userId"));

        if (purchaseService.purchase(userId) > 0) {
            logger.info("Purchased items in the cart (user_id: {})", userId);
            ctx.result("Successfully purchased");
        }
        else {
            logger.warn("Failed to purchase (user_id: {})", userId);
            throw new BadRequestResponse("Couldn't purchase items in the cart");
        }
    }
}
