package dev.kota.models;

import java.io.Serializable;

public class User implements Serializable {
    private int id;
    private boolean isAdmin;

    public User() {super();}

    public User(int id, boolean isAdmin) {
        this.id = id;
        this.isAdmin = isAdmin;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }
}
