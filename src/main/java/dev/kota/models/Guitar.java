package dev.kota.models;

import java.io.Serializable;

public class Guitar implements Serializable {
    private int id;
    private String maker;
    private String name;
    private double price;
    private String condition;

    public Guitar() {
        super();
    }
    public Guitar(int id, String maker, String name, double price, String condition) {
        super();
        this.id = id;
        this.maker = maker;
        this.name = name;
        this.price = price;
        this.condition = condition;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMaker() {
        return maker;
    }

    public void setMaker(String maker) {
        this.maker = maker;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

}
