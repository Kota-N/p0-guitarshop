package dev.kota.data;

import dev.kota.models.Guitar;

import java.util.ArrayList;
import java.util.List;

public class GuitarData {
    private List<Guitar> guitars = new ArrayList<>();

    public GuitarData() {
        super();
        guitars.add(new Guitar(1, "Fender", "Fender X100", 700.00, "new"));
        guitars.add(new Guitar(2, "Martin", "Martin SL14", 1000.00, "new"));
    }

    public List<Guitar> getAllIGuitars() {
        return new ArrayList<>(guitars);
    }

    public Guitar getItemById(int id) {
        return guitars.stream().filter(item -> item.getId() == id).findAny().orElse(null);
    }
}
