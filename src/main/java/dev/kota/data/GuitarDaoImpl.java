package dev.kota.data;

import dev.kota.models.Guitar;
import dev.kota.util.ConnectionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.ConnectException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class GuitarDaoImpl implements GuitarDao {

    private final Logger logger = LoggerFactory.getLogger(GuitarDaoImpl.class);

    private void logException(Exception e){
        logger.error("{} - {}", e.getClass(), e.getMessage());
    }


    @Override
    public List<Guitar> getAllGuitars() {
        List<Guitar> guitars = new ArrayList<>();

        try (Connection connection = ConnectionUtil.getConnection();
             Statement statement = connection.createStatement()) {

            ResultSet rs = statement.executeQuery("select * from guitar");

            while (rs.next()) {
                int id = rs.getInt("id");
                String maker = rs.getString("maker");
                String name = rs.getString("item_name");
                double price = rs.getDouble("price");
                String condition = rs.getString("item_condition");

                guitars.add(new Guitar(id, maker, name, price, condition));
            }

            logger.info("Retrieved {} items from DB", guitars.size());

        } catch (SQLException e) {
            logException(e);
        }

        return guitars;
    }

    @Override
    public Guitar getGuitarById(int id) {
        try (Connection connection = ConnectionUtil.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("select * from guitar where id = ?")) {
            preparedStatement.setInt(1, id);
            ResultSet rs = preparedStatement.executeQuery();

            if (rs.next()) {
                String maker = rs.getString("maker");
                String name = rs.getString("item_name");
                double price = rs.getDouble("price");
                String condition = rs.getString("item_condition");

                logger.info("Retrieved {} from DB", name);
                return new Guitar(id, maker, name, price, condition);
            }
        } catch (SQLException e) {
            logException(e);
        }
        return null;
    }

    @Override
    public int addNewGuitar(Guitar newGuitar) {
        try (Connection connection = ConnectionUtil.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("insert into guitar (maker, item_name, price, item_condition) values (?, ?, ?, ?)");) {

            preparedStatement.setString(1, newGuitar.getMaker());
            preparedStatement.setString(2, newGuitar.getName());
            preparedStatement.setDouble(3, newGuitar.getPrice());
            preparedStatement.setString(4, newGuitar.getCondition());

            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            logException(e);
        }
        return 0;
    }

    @Override
    public int updateGuitarById(int id, Guitar updateGuitar) {
        try (Connection connection = ConnectionUtil.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement("update guitar set maker = ?, item_name = ?, price = ?, item_condition = ? where id = ?")) {

            preparedStatement.setString(1, updateGuitar.getMaker());
            preparedStatement.setString(2, updateGuitar.getName());
            preparedStatement.setDouble(3, updateGuitar.getPrice());
            preparedStatement.setString(4, updateGuitar.getCondition());
            preparedStatement.setInt(5, id);

            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            logException(e);
        }
        return 0;
    }

    @Override
    public int deleteGuitarById(int id) {
        try (Connection connection = ConnectionUtil.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement("delete from guitar where id = ?");) {
            preparedStatement.setInt(1, id);
            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            logException(e);
        }
        return 0;
    }


}
