package dev.kota.data;

import dev.kota.models.User;
import dev.kota.util.ConnectionUtil;

import java.sql.*;

public class UserDaoImpl implements UserDao {
    @Override
    public User loginUser(String email, String password) {
        try (Connection connection = ConnectionUtil.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement("select * from shop_user where email = ? and password = ? ")) {

            preparedStatement.setString(1, email);
            preparedStatement.setString(2, password);
            ResultSet rs = preparedStatement.executeQuery();

            if (rs.next()) return new User(rs.getInt("id"), rs.getBoolean("is_admin"));

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public int signUp(String email, String password) {
        try (Connection connection = ConnectionUtil.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("insert into shop_user (email, password, is_admin) values (?, ?, false);")) {

            preparedStatement.setString(1, email);
            preparedStatement.setString(2, password);

            return preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
