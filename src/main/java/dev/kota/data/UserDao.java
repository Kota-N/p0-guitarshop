package dev.kota.data;

import dev.kota.models.User;

public interface UserDao {
    public User loginUser(String email, String password);
    public int signUp(String email, String password);
}
