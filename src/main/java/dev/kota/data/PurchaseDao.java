package dev.kota.data;

import dev.kota.models.CartItem;

import java.util.List;

public interface PurchaseDao {

    List<CartItem> getCart(int userId);

    int addToCart(int userId, int itemId, int quantity);

    int addToCart(int userId, int itemId);

    int deleteFromCart(int userId, int itemId);

    int purchase(int userId);
}
