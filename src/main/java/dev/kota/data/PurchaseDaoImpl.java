package dev.kota.data;

import dev.kota.models.CartItem;
import dev.kota.util.ConnectionUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PurchaseDaoImpl implements PurchaseDao {

    @Override
    public List<CartItem> getCart(int userId) {
        List<CartItem> cartItems = new ArrayList<>();
        double totalPrice = 0;

        try (Connection connection = ConnectionUtil.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement("select g.id, g.item_name, g.price, c.quantity from guitar g join cart c on c.guitar_id = g.id where c.user_id = ?")) {

            preparedStatement.setInt(1, userId);

            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("item_name");
                double price = rs.getDouble("price");
                int quantity = rs.getInt("quantity");

                totalPrice += price * quantity;

                cartItems.add(new CartItem(id, name, price, quantity));
            }

            cartItems.add(new CartItem(0, "Total Price", totalPrice, 1));

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cartItems;
    }

    @Override
    public int addToCart(int userId, int itemId, int quantity) {

        try (Connection connection = ConnectionUtil.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement("select count(user_id) from cart where user_id = ? and guitar_id = ?")) {

            preparedStatement.setInt(1, userId);
            preparedStatement.setInt(2, itemId);

            ResultSet rs = preparedStatement.executeQuery();
            rs.next();

            if (rs.getInt("count") > 0) {

                try (PreparedStatement preparedStatement2 = connection.prepareStatement("update cart set quantity = (select quantity from cart where user_id = ? and guitar_id = ?) + ? where user_id = ? and guitar_id = ?")) {

                    preparedStatement2.setInt(1, userId);
                    preparedStatement2.setInt(2, itemId);
                    preparedStatement2.setInt(3, quantity);
                    preparedStatement2.setInt(4, userId);
                    preparedStatement2.setInt(5, itemId);

                    return preparedStatement2.executeUpdate();

                }

            } else {
                try (PreparedStatement preparedStatement3 = connection.prepareStatement("insert into cart (user_id, guitar_id, quantity) values (?, ?, ?)")) {

                    preparedStatement3.setInt(1, userId);
                    preparedStatement3.setInt(2, itemId);
                    preparedStatement3.setInt(3, quantity);

                    return preparedStatement3.executeUpdate();

                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return 0;
    }

    @Override
    public int addToCart(int userId, int itemId) {
        try (Connection connection = ConnectionUtil.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement("select count(user_id) from cart where user_id = ? and guitar_id = ?")) {

            preparedStatement.setInt(1, userId);
            preparedStatement.setInt(2, itemId);

            ResultSet rs = preparedStatement.executeQuery();
            rs.next();

            if (rs.getInt("count") > 0) {

                try (PreparedStatement preparedStatement2 = connection.prepareStatement("update cart set quantity = (select quantity from cart where user_id = ? and guitar_id = ?) + 1 where user_id = ? and guitar_id = ?")) {

                    preparedStatement2.setInt(1, userId);
                    preparedStatement2.setInt(2, itemId);
                    preparedStatement2.setInt(3, userId);
                    preparedStatement2.setInt(4, itemId);

                    return preparedStatement2.executeUpdate();

                }

            } else {
                try (PreparedStatement preparedStatement3 = connection.prepareStatement("insert into cart (user_id, guitar_id) values (?, ?)")) {

                    preparedStatement3.setInt(1, userId);
                    preparedStatement3.setInt(2, itemId);

                    return preparedStatement3.executeUpdate();

                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public int deleteFromCart(int userId, int itemId) {

        try (Connection connection = ConnectionUtil.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("delete from cart where user_id = ? and guitar_id = ?")) {

            preparedStatement.setInt(1, userId);
            preparedStatement.setInt(2, itemId);

            return preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }


    @Override
    public int purchase(int userId) {
        List<CartItem> cartItems = getCart(userId);

        String insertInvoiceQuery = "insert into invoice (user_id) values (?)";

        if (cartItems.size() > 1) {

            try (Connection connection = ConnectionUtil.getConnection();
                 PreparedStatement preparedStatement = connection.prepareStatement(insertInvoiceQuery, Statement.RETURN_GENERATED_KEYS)) {

                preparedStatement.setInt(1, userId);

                int affectedRows = preparedStatement.executeUpdate();

                if (affectedRows > 0) {

                    ResultSet rs = preparedStatement.getGeneratedKeys();
                    rs.next();

                    int invoiceId = rs.getInt(1);

                    if (insertInvoiceItem(cartItems, connection, userId, invoiceId) == 0) {
                        deleteInvoice(connection, invoiceId);
                        return 0;
                    }

                }

                return 1;

            } catch (SQLException e) {
                e.printStackTrace();
                return 0;
            }

        }

        return 0;
    }

    private int insertInvoiceItem(List<CartItem> cartItems, Connection connection, int userId, int invoiceId) throws SQLException {

        connection.setAutoCommit(false);

        for (int i = 0; i < cartItems.size() - 1; i++) {

            String insertInvoiceItemQuery = "insert into invoice_item (invoice_id, guitar_id, quantity) values (?, ?, ?); delete from cart where user_id = ? and guitar_id = ?;";

            try (PreparedStatement invoiceItemStatement = connection.prepareStatement(insertInvoiceItemQuery)) {

                int guitarId = cartItems.get(i).getId();
                int quantity = cartItems.get(i).getQuantity();

                invoiceItemStatement.setInt(1, invoiceId);
                invoiceItemStatement.setInt(2, guitarId);
                invoiceItemStatement.setInt(3, quantity);
                invoiceItemStatement.setInt(4, userId);
                invoiceItemStatement.setInt(5, guitarId);

                if (invoiceItemStatement.executeUpdate() == 0) return 0;

            }
        }

        connection.commit();
        connection.setAutoCommit(true);

        return 1;
    }

    private void deleteInvoice(Connection connection, int invoiceId) throws SQLException {

        String deleteQuery = "delete from invoice where id = ?";

        try (PreparedStatement deleteStatement = connection.prepareStatement(deleteQuery)) {
            deleteStatement.setInt(1, invoiceId);
            deleteStatement.executeUpdate();
        }
    }
}
