package dev.kota.data;

import dev.kota.models.Guitar;

import java.util.List;

public interface GuitarDao {
    public List<Guitar> getAllGuitars();
    public Guitar getGuitarById(int id);
    public int addNewGuitar(Guitar newGuitar);
    public int updateGuitarById(int id, Guitar updateGuitar);
    public int deleteGuitarById(int id);
}
